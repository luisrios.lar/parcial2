from pyfirmata import Arduino, util
from tkinter import *
from PIL import Image , ImageTk
from time import sleep

placa = Arduino('COM3')

it = util.Iterator(placa)

it.start()
lect1 = []
lect2 = []
l1 = placa.get_pin('d:8:p')
l2 = placa.get_pin('d:9:p')
l3 = placa.get_pin('d:10:p')
l4 = placa.get_pin('d:11:p')
l5 = placa.get_pin('d:12:p')
l6 = placa.get_pin('d:13:p')

l1.write(0)
l2.write(0)
l3.write(0)
l4.write(0)
l5.write(0)
l6.write(0)

ventana = Tk()
ventana.geometry('500x500')
ventana.configure(bg = 'white')
ventana.title("Punto 2 parcial")
canvas = Canvas(ventana, width = 500, height = 500, bg="white")      
canvas.pack()

logo = PhotoImage(file="logousa.png") 
imagen = Label(ventana, image=logo)
imagen.pack()
imagen.place(x=250,y=330)

def intensidad(dato_lum):
    dato_lum=int(dato_lum)
    if(dato_lum>14):
        canvas.itemconfig(led1, fill="blue")
        canvas.itemconfig(label1, text="Led8", fill="blue")
        l1.write(1)
    else:
        canvas.itemconfig(led1, fill="white")
        canvas.itemconfig(label1, text=" ", fill="white")
        l1.write(0)
    if(dato_lum>28):
        canvas.itemconfig(led2, fill="red")
        canvas.itemconfig(label2, text="Led9", fill="red")
        l2.write(1)
    else:
        canvas.itemconfig(led2, fill="white")
        canvas.itemconfig(label2, text=" ", fill="white")
        l2.write(0)
    if(dato_lum>42):
        canvas.itemconfig(led3, fill="yellow")
        canvas.itemconfig(label3, text="Led19", fill="yellow")
        l3.write(1)
    else:
        canvas.itemconfig(led3, fill="white")
        canvas.itemconfig(label3, text="", fill="white")
        l3.write(0)
    if(dato_lum>56):
        canvas.itemconfig(led4, fill="green")
        canvas.itemconfig(label4, text="Led11", fill="green")
        l4.write(1)
    else:
        canvas.itemconfig(led4, fill="white")
        canvas.itemconfig(label4, text="", fill="white")
        l4.write(0)
    if(dato_lum>70):
        canvas.itemconfig(led5, fill="yellow")
        canvas.itemconfig(label5, text="Led12", fill="yellow")
        l5.write(1)
    else:
        canvas.itemconfig(led5, fill="white")
        canvas.itemconfig(label5, text=" ", fill="white")
        l5.write(0)
    if(dato_lum>84):
        canvas.itemconfig(led6, fill="red")
        canvas.itemconfig(label6, text="Led13", fill="red")
        l6.write(1)
    else:
        canvas.itemconfig(led6, fill="white")
        canvas.itemconfig(label6, text="", fill="white")
        l6.write(0)

slide = Scale(ventana, 
            from_=0, to=100, 
            orient=HORIZONTAL, 
            length= 360,
            label = 'Regular Intensidad',
            bg = 'white',
            font=('Helvetica',12 ),
            fg="blue",
            command= intensidad)
slide.pack()
slide.place(x = 10,y = 90)
led1 = canvas.create_oval(20,20,60,60)
led2 = canvas.create_oval(80,20,120,60)
led3 = canvas.create_oval(140,20,180,60)
led4 = canvas.create_oval(200,20,240,60)
led5 = canvas.create_oval(260,20,300,60)
led6 = canvas.create_oval(320,20,360,60)
label1 = canvas.create_text(40,70,text="",fill="white")
label2 = canvas.create_text(100,70,text="",fill="white")
label3 = canvas.create_text(160,70,text="",fill="white")
label4 = canvas.create_text(220,70,text="",fill="white")
label5 = canvas.create_text(280,70,text="",fill="white")
label6 = canvas.create_text(340,70,text="",fill="white")


ventana.mainloop()