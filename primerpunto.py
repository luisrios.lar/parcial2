
from tkinter import *
from PIL import Image , ImageTk
from time import sleep
cuenta = 0
counter = 0
minutos = 0
horas = 0

ventana = Tk()
ventana.geometry('500x500')
ventana.configure(bg = 'white')
ventana.title("Punto 1 parcial")
canvas = Canvas(ventana, width = 500, height = 500)      
canvas.pack()      


def update_label():
    global cuenta, counter, minutos, horas
    counter += 1
    if counter == 60:
        minutos += 1
        counter = 0
    tiempo['text'] = str(horas).zfill(2)+str(':')+str(minutos).zfill(2)+str(':')+str(counter).zfill(2)

    cuenta=tiempo.after(100, update_label)
   
def pause():
    global cuenta
    tiempo.after_cancel(cuenta)

def reset():
    global cuenta, counter, minutos, horas
    tiempo.after_cancel(cuenta)
    counter = 0
    minutos = 0
    horas = 0

logo = PhotoImage(file="logousa.png") 


Label(ventana, text="Punto 1 parcial", bg='cadet blue1').place(x=210, y=20)

tiempo = Label(ventana, text="00:00:00", bg='cadet blue1', font=("Arial Bold", 14), fg="white")
tiempo.place(x=20, y=90)

imagen = Label(ventana, image=logo)
imagen.pack()
imagen.place(x=250,y=50)

start_button=Button(ventana,text="start",command=update_label)
start_button.pack()
start_button.place(x=30, y=120)
pause=Button(ventana,text="pause",command=pause)
pause.pack()
pause.place(x=30, y=150)
stop=Button(ventana,text="stop",command=reset)
stop.pack()
stop.place(x=30, y=180)
close=Button(ventana,text="close",command=ventana.destroy)
close.pack()
close.place(x=30, y=210)
ventana.mainloop()
