from tkinter import *
from PIL import Image , ImageTk
from time import sleep


ventana = Tk()
ventana.geometry('500x500')
ventana.configure(bg = 'white')
ventana.title("Punto 3 parcial")
canvas = Canvas(ventana, width = 500, height = 500, bg="white")      
canvas.pack()
def balance(dato):
    dato=round(float(dato)*0.360,2)
    canvas.itemconfig(arc, extent=dato)
    canvas.itemconfig(arc2, start=dato, extent=359-int(dato))
    canvas.itemconfig(label1, text="Valor A: "+str(round(100*dato/360,2))+"%")
    canvas.itemconfig(label2, text="Valor B: "+str(round(100-100*dato/360,2))+"%")


def color(dato):
    dato = int(dato)
    hexa = f"0x{dato:02x}"
    last2 = hexa[-2]+hexa[-1]
    canvas.itemconfig(arc, fill="#0000"+last2)

logo = PhotoImage(file="logousa.png") 
imagen = Label(ventana, image=logo)
imagen.pack()
imagen.place(x=250,y=330)
coord = 270, 30, 420, 180
arc = canvas.create_arc(coord, start=0, extent=180, fill="red")
arc2 = canvas.create_arc(coord, start=180, extent=180, fill="green")
slide = Scale(ventana, 
            from_=1, to=999, 
            orient=HORIZONTAL, 
            length= 200,
            label = 'Valor A',
            bg = 'white',
            font=('Helvetica',12 ),
            fg="blue",
            command= balance)
slide.pack()
slide.place(x = 10,y = 90)
slide2 = Scale(ventana, 
            from_=0, to=255, 
            orient=HORIZONTAL, 
            length= 200,
            label = 'Azularidad',
            bg = 'white',
            font=('Helvetica',12 ),
            fg="blue",
            command= color)
slide2.pack()
slide2.place(x = 10,y = 180)
label1 = canvas.create_text(60,30,text="Valor A: 0.2%",fill="black")
label2 = canvas.create_text(60,60,text="Valor B: 99.8%",fill="black")

ventana.mainloop()